import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, TouchableOpacity
} from 'react-native';

import { createStackNavigator } from 'react-navigation'
import TimingAnimation from "./TimingAnimation"
import AnimatedSpring from "./AnimatedSpring"
import DraggableCard from "./DraggableCard"

class App extends Component {

  render() {
    const { navigate } = this.props.navigation
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => navigate('TimingAnimation')}>
          <Text style={styles.welcome}>
            press to check timing animation
        </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigate('AnimatedSpring')}>
          <Text style={styles.welcome}>
            press to check spring animation
        </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigate('DraggableCard')}>
          <Text style={styles.welcome}>
            press to check DraggableCard
        </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default stack = new createStackNavigator({
  App: { screen: App },
  TimingAnimation: { screen: TimingAnimation },
  AnimatedSpring: { screen: AnimatedSpring },
  DraggableCard: { screen: DraggableCard },
})
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
