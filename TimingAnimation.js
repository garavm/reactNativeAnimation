/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, Animated, Easing
} from 'react-native';

export default class TimingAnimation extends Component {
    componentWillMount() {
        this.animatedValue = new Animated.Value(100)
    }
    componentDidMount() {
        Animated.timing(this.animatedValue, {
            toValue: 150,
            duration: 3000,
            easing: Easing.bounce
        }).start()
    }
    render() {
        const animatedStyle = { height: this.animatedValue }
        return (
            <View style={styles.container}>
                <Animated.View style={[styles.box, animatedStyle]} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    box: {
        backgroundColor: '#333',
        width: 100,
        height: 100
    }

});
